import 'package:app_illicado_poc/features/account/data/models/user_model.dart';
import 'package:app_illicado_poc/features/account/domain/entities/commande.dart';
import 'package:app_illicado_poc/features/account/domain/entities/user_account.dart';
import 'package:flutter_test/flutter_test.dart';

import 'commande_model_test.dart';

const UserModel userModel = UserModel(
  creditTotal: 500,
  listCommande: [
    commandeModel,
    commandeModel1,
    commandeModel2,
  ],
  name: "John Doe",
  secondName: "Azerty",
  age: 146,
  mail: "john@test.truc",
);
final Commande commandeEntity = Commande(
  solde: 25,
  dateExpiration: "16/01/2023",
);
final Commande commandeEntity1 = Commande(
  solde: 20,
  dateExpiration: "16/01/2023",
);
final Commande commandeEntity2 = Commande(
  solde: 25,
  dateExpiration: "16/01/2023",
);

void main() {
  test("TEST USER MODEL TO ENTITY MAPPER", () {
    final UserAccount userAccount = UserAccount(
      creditTotal: 500,
      soldeRestant: 350,
      listCommande: [
        commandeEntity,
        commandeEntity1,
        commandeEntity2,
      ],
    );
    // WHEN
    final result = toUserAccountEntity(userModel);
    // THEN
    expect(result.soldeRestant, userAccount.soldeRestant);
    expect(result.creditTotal, userAccount.creditTotal);
    expect(result.listCommande.length, userAccount.listCommande.length);
    expect(result.listCommande.first.solde, userAccount.listCommande.first.solde);
  });
}
