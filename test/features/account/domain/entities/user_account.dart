import 'package:app_illicado_poc/features/account/domain/entities/commande.dart';
import 'package:app_illicado_poc/features/account/domain/entities/user_account.dart';

final userAccount = UserAccount(
  creditTotal: 10,
  soldeRestant: 5,
  listCommande: [
    Commande(
      solde: 5,
      dateExpiration: '10/10/2023',
    ),
  ],
);
