import 'dart:convert';

import 'package:app_illicado_poc/app.dart';
import 'package:app_illicado_poc/core/configuration/configuration.dart';
import 'package:app_illicado_poc/core/providers/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  WidgetsFlutterBinding.ensureInitialized();
  final configuration = await loadConfiguration();

  runApp(
    ProviderScope(
      overrides: [
        configurationProvider.overrideWithValue(configuration),
      ],
      child: const IllicadoApp(),
    ),
  );
}

Future<Configuration> loadConfiguration() async {
  final content = json.decode(
    await rootBundle.loadString('assets/config/config.json'),
  ) as Map<String, Object?>;
  return Configuration.fromJson(content);
}
