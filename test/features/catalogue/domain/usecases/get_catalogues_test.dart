import 'package:app_illicado_poc/features/catalogue/domain/repository/catalogues_repository.dart';
import 'package:app_illicado_poc/features/catalogue/domain/usecases/get_catalogues.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:retrofit/dio.dart';

import '../../../../core/networks/failures.dart';
import '../entities/catalogues.dart';
import 'get_catalogues_test.mocks.dart';

@GenerateMocks(
  [
    CatalogueRepository,
  ],
  customMocks: [
    MockSpec<HttpResponse<Map<String, dynamic>>>(
      as: #MockHttpResponse,
    ),
    MockSpec<HttpResponse<List<Map<String, dynamic>>>>(
      as: #MockHttpResponseList,
    ),
  ],
)
void main() {
  late MockCatalogueRepository mockCatalogueRepository;
  group(
    'GET CATALOGUE TEST',
    (() {
      late GetCatalogues useCaseGetCatalogues;
      setUp(() {
        mockCatalogueRepository = MockCatalogueRepository();
        useCaseGetCatalogues =
            GetCatalogues(repository: mockCatalogueRepository);
      });

      test('TEST USE CASE GET CATALOGUE SUCCESS', () async {
        when(mockCatalogueRepository.getCatlogues())
            .thenAnswer((_) async => Right(catalogues));
        final result = await useCaseGetCatalogues.execute();
        expect(result, Right(catalogues));
        verify(mockCatalogueRepository.getCatlogues());
        verifyNoMoreInteractions(mockCatalogueRepository);
      });

      test('TEST USE CASE GET CATALOGUE FAILURE', () async {
        when(mockCatalogueRepository.getCatlogues())
            .thenAnswer((_) async => Left(failure));
        final result = await useCaseGetCatalogues.execute();
        expect(result, Left(failure));
        verify(mockCatalogueRepository.getCatlogues());
        verifyNoMoreInteractions(mockCatalogueRepository);
      });
    }),
  );
}
