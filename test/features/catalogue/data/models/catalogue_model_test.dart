import 'package:app_illicado_poc/features/catalogue/data/models/catalogue_model.dart';
import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:flutter_test/flutter_test.dart';

const catalogueModelTest = CatalogueModel(
  title: "NOEL",
  year: 1990,
  image: "test.png",
  price: 50,
);

void main() {
  test('Test ENTITY CATALOGUER MAPPER', () {
    final catalogueTest = Catalogue(
      title: "NOEL",
      image: "test.png",
      price: 50,
    );
    final result = cataloguetoEntity(catalogueModel: catalogueModelTest);
    expect(result.title, catalogueTest.title);
    expect(result.image, catalogueTest.image);
    expect(result.price, catalogueTest.price);
  });
}
