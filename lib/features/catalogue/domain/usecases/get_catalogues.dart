import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:app_illicado_poc/features/catalogue/domain/repository/catalogues_repository.dart';
import 'package:dartz/dartz.dart';

class GetCatalogues {
  final CatalogueRepository repository;
  GetCatalogues({required this.repository});

  Future<Either<Failure, List<Catalogue>>> execute() async {
    return await repository.getCatlogues();
  }
}
