import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:flutter/material.dart';

class CatalogueCard extends StatelessWidget {
  final Catalogue catalogue;
  const CatalogueCard({
    required this.catalogue,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green.shade100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(
              12.0,
            ),
            child: Text(
              catalogue.title,
              style: TextStyle(
                  color: Colors.red.shade400,
                  fontSize: 18.0,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Center(
            child: Image.asset(
              catalogue.image,
              width: 300,
              height: 200,
              fit: BoxFit.fill,
            ),
          ),
        ],
      ),
    );
  }
}
