import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:dartz/dartz.dart';

abstract class CatalogueRepository {
  Future<Either<Failure, List<Catalogue>>> getCatlogues();
}
