import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';

final catalogues = [
  Catalogue(
    image: "assets/carte_cadeau_base.png",
    title: 'Noël',
    price: 15,
  ),
  Catalogue(
    image: "assets/carte_cadeau_illicado.png",
    title: 'Anniversaire',
    price: 20,
  ),
  Catalogue(
    image: "assets/carte_illicado.png",
    title: 'Noël',
    price: 25,
  ),
];
