import 'package:app_illicado_poc/features/account/domain/repository/user_account_repository.dart';
import 'package:app_illicado_poc/features/account/domain/usecases/get_user_account.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/networks/failures.dart';
import '../entities/user_account.dart';
import 'get_user_account_test.mocks.dart';

@GenerateMocks([
  UserAccountRepository,
])
void main() {
  late MockUserAccountRepository mockUserAccountRepository;
  group("TEST USER ACCOUNT", () {
    late GetUserAccount getUserAccount;
    setUp(() {
      mockUserAccountRepository = MockUserAccountRepository();
      getUserAccount = GetUserAccount(
        repository: mockUserAccountRepository,
      );
    });
    test("TEST USECASE GET USER ACCOUNT SUCCESSFULL", () async {
      //GIVEN
      when(mockUserAccountRepository.getUserAccount())
          .thenAnswer((_) async => Right(userAccount));
      //WHEN
      final result = await getUserAccount.execute();
      //THEN
      expect(result, Right(userAccount));
      verify(mockUserAccountRepository.getUserAccount());
      verifyNoMoreInteractions(mockUserAccountRepository);
    });

    test("TEST USECASE GET USER ACCOUNT FAILURE", () async {
      //GIVEN
      when(mockUserAccountRepository.getUserAccount())
          .thenAnswer((_) async => Left(failure));
      //WHEN
      final result = await getUserAccount.execute();
      //THEN
      expect(result, Left(failure));
      verify(mockUserAccountRepository.getUserAccount());
      verifyNoMoreInteractions(mockUserAccountRepository);
    });
  });
}
