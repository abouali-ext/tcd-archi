import 'package:flutter/material.dart';
import 'package:app_illicado_poc/core/localization/translate.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('appName'.tr()),
      ),
      body: Center(
        child: ElevatedButton(
            onPressed: () => context.go(
                  '/catalogue',
                ),
            child: Text(
              'appName'.tr(),
            )),
      ),
    );
  }
}
