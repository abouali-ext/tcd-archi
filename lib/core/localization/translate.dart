import 'app_localization.dart';

extension StringTranslate on String {
  String tr({List<String> args = const []}) {
    return AppLocalizations.instance?.translate(this, args) ?? this;
  }
}
