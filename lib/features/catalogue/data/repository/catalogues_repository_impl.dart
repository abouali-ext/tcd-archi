import 'package:app_illicado_poc/core/infra/networks/illicado_api.dart';
import 'package:app_illicado_poc/core/networks/api_status.dart';
import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:app_illicado_poc/features/catalogue/data/models/catalogue_model.dart';
import 'package:app_illicado_poc/features/catalogue/domain/repository/catalogues_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

class CatalogueRepositoryImpl extends CatalogueRepository {
  IllicadoApi apiClient;
  CatalogueRepositoryImpl({required this.apiClient});
  @override
  Future<Either<Failure, List<Catalogue>>> getCatlogues() async {
    try {
      final response = await apiClient.getCatlogues();
      if (response.response.statusCode == ApiStatus.oK) {
        if (response.data != null) {
          final responseCatalogues = response.data as List<dynamic>;
          final catalogues = responseCatalogues
              .map((e) => cataloguetoEntity(
                  catalogueModel:
                      CatalogueModel.fromJson(e as Map<String, dynamic>)))
              .toList();
          return Right(catalogues);
        } else {
          return const Right([]);
        }
      } else {
        return Left(ServerFailure());
      }
    } on DioError catch (e) {
      return Left(ApiFailure(
        code: e.response!.statusCode ?? 0,
      ));
    }
  }
}
