import 'package:app_illicado_poc/features/catalogue/presentation/ui/catalogue_page.dart';
import 'package:go_router/go_router.dart';

final GoRouter appRouter = GoRouter(
  routes: <GoRoute>[
    GoRoute(
      path: '/',
      builder: (context, state) => const CataloguePage(),
      routes: [
        GoRoute(
          path: 'catalogue',
          builder: (context, state) => const CataloguePage(),
        ),
      ],
    )
  ],
);
