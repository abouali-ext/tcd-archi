class Catalogue {
  final String title;
  final String image;
  final double price;

  Catalogue({
    required this.title,
    required this.image,
    required this.price,
  });
}
