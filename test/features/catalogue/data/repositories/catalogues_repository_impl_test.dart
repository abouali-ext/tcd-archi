import 'dart:convert';

import 'package:app_illicado_poc/core/infra/networks/illicado_api.dart';
import 'package:app_illicado_poc/core/networks/api_status.dart';
import 'package:app_illicado_poc/features/catalogue/data/repository/catalogues_repository_impl.dart';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:retrofit/dio.dart';

import 'catalogues_repository_impl_test.mocks.dart';

@GenerateMocks(
  [
    IllicadoApi,
    Response,
  ],
  customMocks: [
    MockSpec<HttpResponse<Map<String, dynamic>>>(
      as: #MockHttpResponse,
    ),
    MockSpec<HttpResponse<List<Map<String, dynamic>>>>(
      as: #MockHttpResponseList,
    ),
  ],
)
void main() {
  group('TEST CATALOGUES REPOSITORY', () {
    late CatalogueRepositoryImpl repositoryImpl;
    late MockIllicadoApi mockApiClient;
    late MockHttpResponseList mockHttpResponse;
    late MockResponse mockResponse;
    setUp(() {
      mockHttpResponse = MockHttpResponseList();
      mockApiClient = MockIllicadoApi();
      mockResponse = MockResponse();
      repositoryImpl = CatalogueRepositoryImpl(
        apiClient: mockApiClient,
      );
    });

    String catalogue = """
      
        {"title": "NOEL", "image": "test.png", "price": 50, "year": 1990}
      
    """;

    List<Map<String, dynamic>> cataloguesModelsMap = [
      jsonDecode(catalogue) as Map<String, dynamic>
    ];

    test('should get catalogues success', () async {
      //GIVEN
      when(
        mockApiClient.getCatlogues(),
      ).thenAnswer(
        (realInvocation) => Future.value(mockHttpResponse),
      );
      when(mockHttpResponse.response).thenReturn(mockResponse);
      when(mockResponse.statusCode).thenReturn(ApiStatus.oK);
      when(mockHttpResponse.data).thenReturn(cataloguesModelsMap);

      //WHEN
      final result = await repositoryImpl.getCatlogues();

      //THEN
      expect(result.isRight(), true);
      verify(mockApiClient.getCatlogues());
      verifyNoMoreInteractions(mockApiClient);
    });
  });
}
