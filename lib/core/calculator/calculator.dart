abstract class CalculatorRepository {
  double getCredit({required double credit, required List<double> depenses});
}