import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:dartz/dartz.dart';

import '../entities/user_account.dart';
import '../repository/user_account_repository.dart';

class GetUserAccount {
  final UserAccountRepository repository;
  GetUserAccount({required this.repository});

  Future<Either<Failure, UserAccount>> execute() async {
    return await repository.getUserAccount();
  }
}
