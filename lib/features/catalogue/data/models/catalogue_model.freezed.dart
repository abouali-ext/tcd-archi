// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'catalogue_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CatalogueModel _$CatalogueModelFromJson(Map<String, dynamic> json) {
  return _CatalogueModel.fromJson(json);
}

/// @nodoc
mixin _$CatalogueModel {
  String get title => throw _privateConstructorUsedError;
  int? get year => throw _privateConstructorUsedError;
  String get image => throw _privateConstructorUsedError;
  double get price => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CatalogueModelCopyWith<CatalogueModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CatalogueModelCopyWith<$Res> {
  factory $CatalogueModelCopyWith(
          CatalogueModel value, $Res Function(CatalogueModel) then) =
      _$CatalogueModelCopyWithImpl<$Res, CatalogueModel>;
  @useResult
  $Res call({String title, int? year, String image, double price});
}

/// @nodoc
class _$CatalogueModelCopyWithImpl<$Res, $Val extends CatalogueModel>
    implements $CatalogueModelCopyWith<$Res> {
  _$CatalogueModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? year = freezed,
    Object? image = null,
    Object? price = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      year: freezed == year
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as int?,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      price: null == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CatalogueModelCopyWith<$Res>
    implements $CatalogueModelCopyWith<$Res> {
  factory _$$_CatalogueModelCopyWith(
          _$_CatalogueModel value, $Res Function(_$_CatalogueModel) then) =
      __$$_CatalogueModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title, int? year, String image, double price});
}

/// @nodoc
class __$$_CatalogueModelCopyWithImpl<$Res>
    extends _$CatalogueModelCopyWithImpl<$Res, _$_CatalogueModel>
    implements _$$_CatalogueModelCopyWith<$Res> {
  __$$_CatalogueModelCopyWithImpl(
      _$_CatalogueModel _value, $Res Function(_$_CatalogueModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? year = freezed,
    Object? image = null,
    Object? price = null,
  }) {
    return _then(_$_CatalogueModel(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      year: freezed == year
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as int?,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      price: null == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CatalogueModel extends _CatalogueModel {
  const _$_CatalogueModel(
      {required this.title,
      this.year,
      required this.image,
      required this.price})
      : super._();

  factory _$_CatalogueModel.fromJson(Map<String, dynamic> json) =>
      _$$_CatalogueModelFromJson(json);

  @override
  final String title;
  @override
  final int? year;
  @override
  final String image;
  @override
  final double price;

  @override
  String toString() {
    return 'CatalogueModel(title: $title, year: $year, image: $image, price: $price)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CatalogueModel &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.year, year) || other.year == year) &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.price, price) || other.price == price));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, title, year, image, price);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CatalogueModelCopyWith<_$_CatalogueModel> get copyWith =>
      __$$_CatalogueModelCopyWithImpl<_$_CatalogueModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CatalogueModelToJson(
      this,
    );
  }
}

abstract class _CatalogueModel extends CatalogueModel {
  const factory _CatalogueModel(
      {required final String title,
      final int? year,
      required final String image,
      required final double price}) = _$_CatalogueModel;
  const _CatalogueModel._() : super._();

  factory _CatalogueModel.fromJson(Map<String, dynamic> json) =
      _$_CatalogueModel.fromJson;

  @override
  String get title;
  @override
  int? get year;
  @override
  String get image;
  @override
  double get price;
  @override
  @JsonKey(ignore: true)
  _$$_CatalogueModelCopyWith<_$_CatalogueModel> get copyWith =>
      throw _privateConstructorUsedError;
}
