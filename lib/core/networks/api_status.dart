class ApiStatus {
  static const int oK = 200;
  static const int ressourceNotFound = 404;
  static const int serverError = 500;
}
