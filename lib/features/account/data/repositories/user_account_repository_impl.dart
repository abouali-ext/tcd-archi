import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:app_illicado_poc/features/account/data/models/commande_model.dart';
import 'package:app_illicado_poc/features/account/data/models/user_model.dart';
import 'package:app_illicado_poc/features/account/domain/entities/user_account.dart';
import 'package:app_illicado_poc/features/account/domain/repository/user_account_repository.dart';
import 'package:dartz/dartz.dart';

class UserAccountRepositoryImpl extends UserAccountRepository {
  @override
  Future<Either<Failure, UserAccount>> getUserAccount() async {
    const UserModel userModel = UserModel(
      creditTotal: 200,
      listCommande: [
        CommandeModel(
          credit: 100,
          depenses: [20],
          dateExpiration: "19/01/2023",
        ),
        CommandeModel(
          credit: 100,
          depenses: [30],
          dateExpiration: "19/01/2023",
        ),
      ],
      name: "John Doe",
      secondName: "Doe",
      age: 110,
      mail: "john.doe@test.fr",
    );
    UserAccount userAccount = toUserAccountEntity(userModel);

    return await Future.delayed(Duration.zero, () => Right(userAccount));
  }
}
