// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'catalogue_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CatalogueModel _$$_CatalogueModelFromJson(Map<String, dynamic> json) =>
    _$_CatalogueModel(
      title: json['title'] as String,
      year: json['year'] as int?,
      image: json['image'] as String,
      price: (json['price'] as num).toDouble(),
    );

Map<String, dynamic> _$$_CatalogueModelToJson(_$_CatalogueModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'year': instance.year,
      'image': instance.image,
      'price': instance.price,
    };
