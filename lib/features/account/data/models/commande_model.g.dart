// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'commande_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CommandeModel _$$_CommandeModelFromJson(Map<String, dynamic> json) =>
    _$_CommandeModel(
      credit: (json['credit'] as num).toDouble(),
      dateExpiration: json['dateExpiration'] as String,
      depenses: (json['depenses'] as List<dynamic>)
          .map((e) => (e as num).toDouble())
          .toList(),
    );

Map<String, dynamic> _$$_CommandeModelToJson(_$_CommandeModel instance) =>
    <String, dynamic>{
      'credit': instance.credit,
      'dateExpiration': instance.dateExpiration,
      'depenses': instance.depenses,
    };
