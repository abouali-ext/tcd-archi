// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return _UserModel.fromJson(json);
}

/// @nodoc
mixin _$UserModel {
  double get creditTotal => throw _privateConstructorUsedError;
  List<CommandeModel> get listCommande => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get secondName => throw _privateConstructorUsedError;
  int get age => throw _privateConstructorUsedError;
  String get mail => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserModelCopyWith<UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserModelCopyWith<$Res> {
  factory $UserModelCopyWith(UserModel value, $Res Function(UserModel) then) =
      _$UserModelCopyWithImpl<$Res, UserModel>;
  @useResult
  $Res call(
      {double creditTotal,
      List<CommandeModel> listCommande,
      String name,
      String secondName,
      int age,
      String mail});
}

/// @nodoc
class _$UserModelCopyWithImpl<$Res, $Val extends UserModel>
    implements $UserModelCopyWith<$Res> {
  _$UserModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? creditTotal = null,
    Object? listCommande = null,
    Object? name = null,
    Object? secondName = null,
    Object? age = null,
    Object? mail = null,
  }) {
    return _then(_value.copyWith(
      creditTotal: null == creditTotal
          ? _value.creditTotal
          : creditTotal // ignore: cast_nullable_to_non_nullable
              as double,
      listCommande: null == listCommande
          ? _value.listCommande
          : listCommande // ignore: cast_nullable_to_non_nullable
              as List<CommandeModel>,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      secondName: null == secondName
          ? _value.secondName
          : secondName // ignore: cast_nullable_to_non_nullable
              as String,
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      mail: null == mail
          ? _value.mail
          : mail // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserModelCopyWith<$Res> implements $UserModelCopyWith<$Res> {
  factory _$$_UserModelCopyWith(
          _$_UserModel value, $Res Function(_$_UserModel) then) =
      __$$_UserModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {double creditTotal,
      List<CommandeModel> listCommande,
      String name,
      String secondName,
      int age,
      String mail});
}

/// @nodoc
class __$$_UserModelCopyWithImpl<$Res>
    extends _$UserModelCopyWithImpl<$Res, _$_UserModel>
    implements _$$_UserModelCopyWith<$Res> {
  __$$_UserModelCopyWithImpl(
      _$_UserModel _value, $Res Function(_$_UserModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? creditTotal = null,
    Object? listCommande = null,
    Object? name = null,
    Object? secondName = null,
    Object? age = null,
    Object? mail = null,
  }) {
    return _then(_$_UserModel(
      creditTotal: null == creditTotal
          ? _value.creditTotal
          : creditTotal // ignore: cast_nullable_to_non_nullable
              as double,
      listCommande: null == listCommande
          ? _value.listCommande
          : listCommande // ignore: cast_nullable_to_non_nullable
              as List<CommandeModel>,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      secondName: null == secondName
          ? _value.secondName
          : secondName // ignore: cast_nullable_to_non_nullable
              as String,
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      mail: null == mail
          ? _value.mail
          : mail // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserModel extends _UserModel {
  const _$_UserModel(
      {required this.creditTotal,
      required this.listCommande,
      required this.name,
      required this.secondName,
      required this.age,
      required this.mail})
      : super._();

  factory _$_UserModel.fromJson(Map<String, dynamic> json) =>
      _$$_UserModelFromJson(json);

  @override
  final double creditTotal;
  @override
  final List<CommandeModel> listCommande;
  @override
  final String name;
  @override
  final String secondName;
  @override
  final int age;
  @override
  final String mail;

  @override
  String toString() {
    return 'UserModel(creditTotal: $creditTotal, listCommande: $listCommande, name: $name, secondName: $secondName, age: $age, mail: $mail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserModel &&
            (identical(other.creditTotal, creditTotal) ||
                other.creditTotal == creditTotal) &&
            const DeepCollectionEquality()
                .equals(other.listCommande, listCommande) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.secondName, secondName) ||
                other.secondName == secondName) &&
            (identical(other.age, age) || other.age == age) &&
            (identical(other.mail, mail) || other.mail == mail));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      creditTotal,
      const DeepCollectionEquality().hash(listCommande),
      name,
      secondName,
      age,
      mail);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      __$$_UserModelCopyWithImpl<_$_UserModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserModelToJson(
      this,
    );
  }
}

abstract class _UserModel extends UserModel {
  const factory _UserModel(
      {required final double creditTotal,
      required final List<CommandeModel> listCommande,
      required final String name,
      required final String secondName,
      required final int age,
      required final String mail}) = _$_UserModel;
  const _UserModel._() : super._();

  factory _UserModel.fromJson(Map<String, dynamic> json) =
      _$_UserModel.fromJson;

  @override
  double get creditTotal;
  @override
  List<CommandeModel> get listCommande;
  @override
  String get name;
  @override
  String get secondName;
  @override
  int get age;
  @override
  String get mail;
  @override
  @JsonKey(ignore: true)
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}
