import 'package:app_illicado_poc/core/providers/providers.dart';
import 'package:app_illicado_poc/features/account/domain/entities/commande.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'commande_model.freezed.dart';
part 'commande_model.g.dart';

@Freezed()
class CommandeModel with _$CommandeModel {
  const factory CommandeModel({
    required double credit,
    required String dateExpiration,
    required List<double> depenses,
  }) = _CommandeModel;

  const CommandeModel._();

  factory CommandeModel.fromJson(Map<String, dynamic> json) =>
    _$CommandeModelFromJson(json);
}

Commande toCommandeEntity({required CommandeModel commandeModel}) => Commande(
  solde: calculator.getCredit(
    credit: commandeModel.credit,
    depenses: commandeModel.depenses
  ),
  dateExpiration: commandeModel.dateExpiration
);