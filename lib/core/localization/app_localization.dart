import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

List<Locale> appLocales = [
  const Locale('en'),
  const Locale('fr'),
  const Locale('it'),
  const Locale('es'),
];

const appLocalizationDelegates = [
  AppLocalizations.delegate,
  // Built-in localization of basic text for Material widgets
  GlobalMaterialLocalizations.delegate,
  // Built-in localization for text direction LTR/RTL
  GlobalWidgetsLocalizations.delegate,
];

class AppLocalizations {
  static const LocalizationsDelegate<AppLocalizations> delegate =
      _AppLocalizationsDelegate();

  static AppLocalizations? instance;

  final Locale locale;
  final Locale fallbackLocale = appLocales.first;

  AppLocalizations(this.locale) {
    instance = this;
  }

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  Map<String, String?>? _localizedStrings;
  Map<String, String?>? _fallbackStrings;

  Future<bool> load() async {
    _localizedStrings = await _load(locale);
    _fallbackStrings = await _load(fallbackLocale);
    return true;
  }

  Future<Map<String, String>> _load(Locale locale) async {
    final String asset = 'assets/translations/${locale.languageCode}.json';
    debugPrint('loading asset $asset');
    final String jsonString = await rootBundle.loadString(asset);
    final Map<String, dynamic> jsonMap =
        json.decode(jsonString) as Map<String, dynamic>;
    return jsonMap.map((key, value) => MapEntry(key, value.toString()));
  }

  String translate(String key, List<String> args) {
    String? translation = _localizedStrings![key];
    translation ??= _fallbackStrings![key] ?? "[$key]";

    for (final String arg in args) {
      translation = translation!.replaceFirst(RegExp('{}'), arg);
    }
    return translation!;
  }

  Map<String, String?>? get localizedStrings => _localizedStrings;

  Map<String, String?>? get fallbackStrings => _fallbackStrings;
}

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return appLocales
        .map((appLocale) => appLocale.languageCode)
        .contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    debugPrint('loading locale ${locale.languageCode}');
    final AppLocalizations localizations = AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
