import 'package:app_illicado_poc/features/account/domain/entities/commande.dart';

class UserAccount {
  final double creditTotal;
  final double soldeRestant;
  final List<Commande> listCommande;

  UserAccount({
    required this.creditTotal,
    required this.soldeRestant,
    required this.listCommande,
  });
}
