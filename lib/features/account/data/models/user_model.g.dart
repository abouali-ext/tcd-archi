// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserModel _$$_UserModelFromJson(Map<String, dynamic> json) => _$_UserModel(
      creditTotal: (json['creditTotal'] as num).toDouble(),
      listCommande: (json['listCommande'] as List<dynamic>)
          .map((e) => CommandeModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      name: json['name'] as String,
      secondName: json['secondName'] as String,
      age: json['age'] as int,
      mail: json['mail'] as String,
    );

Map<String, dynamic> _$$_UserModelToJson(_$_UserModel instance) =>
    <String, dynamic>{
      'creditTotal': instance.creditTotal,
      'listCommande': instance.listCommande,
      'name': instance.name,
      'secondName': instance.secondName,
      'age': instance.age,
      'mail': instance.mail,
    };
