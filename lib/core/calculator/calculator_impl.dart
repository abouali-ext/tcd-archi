import 'package:app_illicado_poc/core/calculator/calculator.dart';

class CalculatorImpl extends CalculatorRepository {
  @override
  double getCredit({required double credit, required List<double> depenses}) {
    return credit - depenses.reduce((value, element) => value + element).toDouble();
  }
}