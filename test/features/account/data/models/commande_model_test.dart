import 'package:app_illicado_poc/features/account/data/models/commande_model.dart';
import 'package:app_illicado_poc/features/account/domain/entities/commande.dart';
import 'package:flutter_test/flutter_test.dart';

const CommandeModel commandeModel = CommandeModel(
  credit: 50,
  dateExpiration: "16/01/2023",
  depenses: [10, 15],
);
const CommandeModel commandeModel1 = CommandeModel(
  credit: 40,
  dateExpiration: "16/01/2023",
  depenses: [10, 10],
);
const CommandeModel commandeModel2 = CommandeModel(
  credit: 60,
  dateExpiration: "16/01/2023",
  depenses: [20, 15],
);
final Commande commandeEntity = Commande(
  solde: 25,
  dateExpiration: "16/01/2023",
);

void main() {
  test("TEST COMMANDE MODEL TO ENTITY MAPPER", () {
    // GIVEN

    // WHEN
    final result = toCommandeEntity(commandeModel: commandeModel);
    // THEN
    expect(result.solde, commandeEntity.solde);
    expect(result.dateExpiration, commandeEntity.dateExpiration);
  });
}
