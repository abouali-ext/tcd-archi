import 'package:app_illicado_poc/core/providers/providers.dart';
import 'package:app_illicado_poc/features/account/data/models/commande_model.dart';
import 'package:app_illicado_poc/features/account/domain/entities/user_account.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@Freezed(makeCollectionsUnmodifiable: false)
class UserModel with _$UserModel {
  const factory UserModel({
    required double creditTotal,
    required List<CommandeModel> listCommande,
    required String name,
    required String secondName,
    required int age,
    required String mail,
  }) = _UserModel;

  const UserModel._();

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);
}

UserAccount toUserAccountEntity(UserModel userModel) => UserAccount(
  creditTotal: userModel.creditTotal,
  soldeRestant: calculator.getCredit(
    credit: userModel.creditTotal,
    depenses: userModel.listCommande.map((e) => e.credit).toList(),
  ),
  listCommande: userModel.listCommande
    .map((commandeModel) => toCommandeEntity(commandeModel: commandeModel)).toList(),
);