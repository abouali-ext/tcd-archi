import 'package:app_illicado_poc/router.dart';
import 'package:flutter/material.dart';
import 'package:app_illicado_poc/core/localization/translate.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

class IllicadoApp extends StatelessWidget {
  const IllicadoApp({super.key});

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();

    return MaterialApp.router(
      title: 'appName'.tr(),
      routerConfig: appRouter,
    );
  }
}
