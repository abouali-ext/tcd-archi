import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'catalogue_model.freezed.dart';
part 'catalogue_model.g.dart';

@Freezed(
  makeCollectionsUnmodifiable: false,
)
class CatalogueModel with _$CatalogueModel {
  const factory CatalogueModel({
    required String title,
    int? year,
    required String image,
    required double price,
  }) = _CatalogueModel;
  const CatalogueModel._();

  factory CatalogueModel.fromJson(Map<String, dynamic> json) =>
      _$CatalogueModelFromJson(json);
}

Catalogue cataloguetoEntity({
  CatalogueModel? catalogueModel,
}) =>
    Catalogue(
      title: catalogueModel?.title ?? "",
      image: catalogueModel?.image ?? "",
      price: catalogueModel?.price ?? 0.0,
    );
