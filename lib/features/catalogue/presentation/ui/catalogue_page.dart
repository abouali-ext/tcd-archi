import 'package:app_illicado_poc/core/providers/providers.dart';
import 'package:app_illicado_poc/features/catalogue/presentation/widgets/catalogue_card.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CataloguePage extends ConsumerWidget {
  const CataloguePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    CarouselController buttonCarouselController = CarouselController();

    final catalogues = ref.watch(getCatalogueProvider);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: catalogues.isNotEmpty
          ? CarouselSlider(
              items: catalogues
                  .map(
                    (catalogue) => CatalogueCard(catalogue: catalogue),
                  )
                  .toList(),
              carouselController: buttonCarouselController,
              options: CarouselOptions(
                height: 270,
                autoPlay: true,
                autoPlayAnimationDuration: const Duration(
                  microseconds: 300,
                ),
                aspectRatio: 16 / 9,
                viewportFraction: 0.8,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlayInterval: const Duration(
                  milliseconds: 300,
                ),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                enlargeFactor: 0.3,
                scrollDirection: Axis.horizontal,
              ),
            )
          : const Center(
              child: Text(
              'EMPTY LIST',
              style: TextStyle(
                color: Colors.black,
              ),
            )),
    );
  }
}
