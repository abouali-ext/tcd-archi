import 'package:app_illicado_poc/core/calculator/calculator.dart';
import 'package:app_illicado_poc/core/calculator/calculator_impl.dart';
import 'package:app_illicado_poc/core/configuration/configuration.dart';
import 'package:app_illicado_poc/core/infra/networks/illicado_api.dart';
import 'package:app_illicado_poc/features/catalogue/data/repository/catalogues_repository_impl.dart';
import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:app_illicado_poc/features/catalogue/domain/usecases/get_catalogues.dart';
import 'package:app_illicado_poc/features/catalogue/presentation/states/catalogue_notifier.dart';
import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final configurationProvider = Provider<Configuration>((ref) {
  return Configuration.fromJson({});
});

final dioProvider = Provider(
  (ref) => Dio(),
);

final illicadoApiProvider = Provider(
  (ref) => IllicadoApi(
    ref.read(
      dioProvider,
    ),
    baseUrl: ref.read(configurationProvider).backendUrl,
  ),
);

final catalogueReposioryProvider = Provider(
  (ref) => CatalogueRepositoryImpl(
    apiClient: ref.read(
      illicadoApiProvider,
    ),
  ),
);

final getCatalogueUseCaseProvider = Provider<GetCatalogues>(
  (ref) => GetCatalogues(
    repository: ref.read(
      catalogueReposioryProvider,
    ),
  ),
);

final getCatalogueProvider =
    StateNotifierProvider<CatalogueNotifier, List<Catalogue>>(
  (ref) => CatalogueNotifier(
    getCataloguesUseCase: ref.read(getCatalogueUseCaseProvider),
  )..getCatalogues(),
);

final CalculatorRepository calculator = CalculatorImpl();
