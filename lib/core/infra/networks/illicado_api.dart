import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'illicado_api.g.dart';

@RestApi(baseUrl: 'http://localhost:8080/')
abstract class IllicadoApi {
  factory IllicadoApi(
    Dio dio, {
    String baseUrl,
  }) = _IllicadoApi;

  @GET("/catalogues")
  Future<HttpResponse> getCatlogues();
}
