import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:app_illicado_poc/features/account/domain/entities/user_account.dart';
import 'package:app_illicado_poc/features/account/domain/usecases/get_user_account.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class UserNotifier extends StateNotifier<UserAccount?> {
  // Constructor
  UserNotifier({required this.getUserAccountUseCase}) : super(null);

  // Properties
  final GetUserAccount getUserAccountUseCase;

  // Methods
  Future<void> getUserAccount() async {
    try {
      final data = await getUserAccountUseCase.execute();
      data.fold(
        (failure) => state = null,
        (user) => state = user,
      );
    }
    on Failure {
      state = null;
    }
  }
}