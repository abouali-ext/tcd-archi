import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:dartz/dartz.dart';

import '../entities/user_account.dart';

abstract class UserAccountRepository {
  Future<Either<Failure, UserAccount>> getUserAccount();
}
