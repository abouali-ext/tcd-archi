import 'package:app_illicado_poc/core/networks/failure.dart';
import 'package:app_illicado_poc/features/catalogue/domain/entities/catalogue.dart';
import 'package:app_illicado_poc/features/catalogue/domain/usecases/get_catalogues.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CatalogueNotifier extends StateNotifier<List<Catalogue>> {
  final GetCatalogues getCataloguesUseCase;
  CatalogueNotifier({required this.getCataloguesUseCase}) : super([]);

  Future<void> getCatalogues() async {
    try {
      final data = await getCataloguesUseCase.execute();
      data.fold(
        (failure) => state = [],
        (catalogues) => state = catalogues,
      );
    } on Failure {
      state = [];
    }
  }
}
