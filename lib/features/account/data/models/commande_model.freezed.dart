// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'commande_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CommandeModel _$CommandeModelFromJson(Map<String, dynamic> json) {
  return _CommandeModel.fromJson(json);
}

/// @nodoc
mixin _$CommandeModel {
  double get credit => throw _privateConstructorUsedError;
  String get dateExpiration => throw _privateConstructorUsedError;
  List<double> get depenses => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CommandeModelCopyWith<CommandeModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CommandeModelCopyWith<$Res> {
  factory $CommandeModelCopyWith(
          CommandeModel value, $Res Function(CommandeModel) then) =
      _$CommandeModelCopyWithImpl<$Res, CommandeModel>;
  @useResult
  $Res call({double credit, String dateExpiration, List<double> depenses});
}

/// @nodoc
class _$CommandeModelCopyWithImpl<$Res, $Val extends CommandeModel>
    implements $CommandeModelCopyWith<$Res> {
  _$CommandeModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? credit = null,
    Object? dateExpiration = null,
    Object? depenses = null,
  }) {
    return _then(_value.copyWith(
      credit: null == credit
          ? _value.credit
          : credit // ignore: cast_nullable_to_non_nullable
              as double,
      dateExpiration: null == dateExpiration
          ? _value.dateExpiration
          : dateExpiration // ignore: cast_nullable_to_non_nullable
              as String,
      depenses: null == depenses
          ? _value.depenses
          : depenses // ignore: cast_nullable_to_non_nullable
              as List<double>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CommandeModelCopyWith<$Res>
    implements $CommandeModelCopyWith<$Res> {
  factory _$$_CommandeModelCopyWith(
          _$_CommandeModel value, $Res Function(_$_CommandeModel) then) =
      __$$_CommandeModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double credit, String dateExpiration, List<double> depenses});
}

/// @nodoc
class __$$_CommandeModelCopyWithImpl<$Res>
    extends _$CommandeModelCopyWithImpl<$Res, _$_CommandeModel>
    implements _$$_CommandeModelCopyWith<$Res> {
  __$$_CommandeModelCopyWithImpl(
      _$_CommandeModel _value, $Res Function(_$_CommandeModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? credit = null,
    Object? dateExpiration = null,
    Object? depenses = null,
  }) {
    return _then(_$_CommandeModel(
      credit: null == credit
          ? _value.credit
          : credit // ignore: cast_nullable_to_non_nullable
              as double,
      dateExpiration: null == dateExpiration
          ? _value.dateExpiration
          : dateExpiration // ignore: cast_nullable_to_non_nullable
              as String,
      depenses: null == depenses
          ? _value._depenses
          : depenses // ignore: cast_nullable_to_non_nullable
              as List<double>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CommandeModel extends _CommandeModel {
  const _$_CommandeModel(
      {required this.credit,
      required this.dateExpiration,
      required final List<double> depenses})
      : _depenses = depenses,
        super._();

  factory _$_CommandeModel.fromJson(Map<String, dynamic> json) =>
      _$$_CommandeModelFromJson(json);

  @override
  final double credit;
  @override
  final String dateExpiration;
  final List<double> _depenses;
  @override
  List<double> get depenses {
    if (_depenses is EqualUnmodifiableListView) return _depenses;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_depenses);
  }

  @override
  String toString() {
    return 'CommandeModel(credit: $credit, dateExpiration: $dateExpiration, depenses: $depenses)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CommandeModel &&
            (identical(other.credit, credit) || other.credit == credit) &&
            (identical(other.dateExpiration, dateExpiration) ||
                other.dateExpiration == dateExpiration) &&
            const DeepCollectionEquality().equals(other._depenses, _depenses));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, credit, dateExpiration,
      const DeepCollectionEquality().hash(_depenses));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CommandeModelCopyWith<_$_CommandeModel> get copyWith =>
      __$$_CommandeModelCopyWithImpl<_$_CommandeModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CommandeModelToJson(
      this,
    );
  }
}

abstract class _CommandeModel extends CommandeModel {
  const factory _CommandeModel(
      {required final double credit,
      required final String dateExpiration,
      required final List<double> depenses}) = _$_CommandeModel;
  const _CommandeModel._() : super._();

  factory _CommandeModel.fromJson(Map<String, dynamic> json) =
      _$_CommandeModel.fromJson;

  @override
  double get credit;
  @override
  String get dateExpiration;
  @override
  List<double> get depenses;
  @override
  @JsonKey(ignore: true)
  _$$_CommandeModelCopyWith<_$_CommandeModel> get copyWith =>
      throw _privateConstructorUsedError;
}
